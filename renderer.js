var $ = require('jquery');
var dt = require('datatables.net')(window, $);
var buttons = require( 'datatables.net-buttons' )( window, $ );
var Swal = require('sweetalert2');
var Chart = require('chart.js');

//pega o conteudo das paginas

var ajaxPainelPrincipalView = new XMLHttpRequest();
ajaxPainelPrincipalView.open("GET", "views/painel_principal/painel_principal.html", false);
ajaxPainelPrincipalView.send();

var ajaxVendasView = new XMLHttpRequest();
ajaxVendasView.open("GET", "views/vendas/vendas.html", false);
ajaxVendasView.send();

var ajaxProdutosView = new XMLHttpRequest();
ajaxProdutosView.open("GET", "views/produtos/produtos.html", false);
ajaxProdutosView.send();

var ajaxClientesView = new XMLHttpRequest();
ajaxClientesView.open("GET", "views/clientes/clientes.html", false);
ajaxClientesView.send();

var ajaxContasReceberView = new XMLHttpRequest();
ajaxContasReceberView.open("GET", "views/contas_receber/contas_receber.html", false);
ajaxContasReceberView.send();

var ajaxConfiguracoesView = new XMLHttpRequest();
ajaxConfiguracoesView.open("GET", "views/configuracoes/configuracoes.html", false);
ajaxConfiguracoesView.send();


//passa os conteudos das paginas para constantes

const PainelPrincipalView = {template: ajaxPainelPrincipalView.responseText}
const VendasView = {template: ajaxVendasView.responseText}
const ProdutosView = {template: ajaxProdutosView.responseText}
const ClientesView = {template: ajaxClientesView.responseText}
const ContasReceberView = {template: ajaxContasReceberView.responseText}
const ConfiguracoesView = {template: ajaxConfiguracoesView.responseText}


//define as rotas para o Vue Router

const routes = [
    {path: '/PainelPrincipalView', component: PainelPrincipalView},
    {path: '/VendasView', component: VendasView},
    {path: '/ProdutosView', component: ProdutosView},
    {path: '/ClientesView', component: ClientesView},
    {path: '/ContasReceberView', component: ContasReceberView},
    {path: '/ConfiguracoesView', component: ConfiguracoesView},
];

const router = new VueRouter({
    routes
})

const tudo = new Vue({
    router
}).$mount('#tudo');


//carrega javaScript dos menus

$('#menu_painel_principal').click(function () {
    $.getScript("views/painel_principal/painel_principal.js", function () {
    });
});

$('#menu_vendas').click(function () {
    $.getScript("views/vendas/vendas.js", function () {
    });
});

$('#menu_produtos').click(function () {
    $.getScript("views/produtos/produtos.js", function () {
    });
});

$('#menu_clientes').click(function () {
    $.getScript("views/clientes/clientes.js", function () {
    });
});

$('#menu_clientes').click(function () {
    $.getScript("views/contas_receber/contas_receber.js", function () {
    });
});

$('#menu_configuracoes').click(function () {
    $.getScript("views/configuracoes/configuracoes.js", function () {
    });
});

//inicializa com o sistema na tela painel principal

window.onload = function () {
    document.getElementById('menu_painel_principal').click();
    $.getScript("js/script.js", function () {
    });
    $.getScript("js/modal.js", function () {
    });
}

//remove classe ativa dos menus a atribui a classe ativa para o item do menu atual

$('.menu').click(function () {
    $('.menu').each(function (index) {
        $(this).removeClass('active');
    });
    $(this).addClass('active');
    $('#menu_atual').val($(this).text());
})
