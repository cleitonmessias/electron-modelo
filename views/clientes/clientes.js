$(document).ready(function () {

    //inicializa a tabela
    function clientes_table_get() {
        db.transaction(function (tx) {
            tx.executeSql("SELECT * FROM clientes", [], function (tx, retultado) {
                var rows = retultado.rows;
                console.log(rows);

                $('#clientes_table').DataTable().destroy()

                $('#clientes_table').DataTable({
                    language: {
                        url: "outros/datatableBrasil.json"
                    },
                    data: rows,
                    "columns": [
                        {"data": "id"},
                        {"data": "nome"},
                        {"data": "cpf"},
                        {"data": "telefone"},
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            className: ['buttonSave', 'btn'],
                            text: 'Novo',
                            action: function (e, dt, node, config) {
                                $('#cliente_modal_novo').modal({
                                    showClose: false
                                });
                            }
                        }
                    ]
                });
            });
        })
    }

    clientes_table_get();

    //cadastra novo cliente
    $('#cliente_modal_novo_salvar').click(function () {

        if (notNull($('#cliente_modal_novo_nome').val(), 'campo nome não pode ser vazio')) {

            db.transaction(function (tx) {

                tx.executeSql('INSERT INTO clientes (nome,cpf,telefone,create_at) VALUES (?,?,?,?)',
                    [$('#cliente_modal_novo_nome').val(), $('#cliente_modal_novo_cpf').val(), $('#cliente_modal_novo_telefone').val(), new Date().getTime()],
                    function (transaction, result) {
                        Swal.fire({
                            type: 'success',
                            title: 'Sucesso',
                            text: 'Cadastro realizado com sucesso',
                        })
                        $('#cliente_modal_novo_nome').val('');
                        $('#cliente_modal_novo_cpf').val('');
                        $('#cliente_modal_novo_telefone').val('');
                        clientes_table_get();
                    },
                    function (transaction, error) {
                        Swal.fire({
                            type: 'error',
                            title: 'Erro...',
                            text: 'Erro ao realizar o cadastro!',
                        });
                        $('#cliente_modal_novo').modal({
                            showClose: false
                        });
                    });
            });
        }
    });

});
