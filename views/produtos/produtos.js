$(document).ready(function () {

    //inicializa a tabela
    function produtos_table_get() {
        db.transaction(function (tx) {
            tx.executeSql("SELECT * FROM produtos", [], function (tx, retultado) {
                var rows = retultado.rows;
                console.log(rows);

                $('#produtos_table').DataTable().destroy()

                $('#produtos_table').DataTable({
                    language: {
                        url: "outros/datatableBrasil.json"
                    },
                    data: rows,
                    "columns": [
                        {"data": "id"},
                        {"data": "nome"},
                        {"data": "preco_custo"},
                        {"data": "preco_venda"},
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            className: ['buttonSave', 'btn'],
                            text: 'Novo',
                            action: function (e, dt, node, config) {
                                $('#produtos_modal_novo').modal({
                                    showClose: false
                                });
                            }
                        }
                    ]
                });
            });
        })
    }

    produtos_table_get();

    //cadastra novo produto
    $('#produtos_modal_novo_salvar').click(function () {

        if (notNull($('#produtos_modal_novo_nome').val(), 'campo nome não pode ser vazio')) {

            db.transaction(function (tx) {

                tx.executeSql('INSERT INTO produtos (nome,preco_custo,preco_venda,create_at) VALUES (?,?,?,?)',
                    [$('#produtos_modal_novo_nome').val(), $('#produtos_modal_novo_preco_custo').val(), $('#produtos_modal_novo_preco_venda').val(), new Date().getTime()],
                    function (transaction, result) {
                        Swal.fire({
                            type: 'success',
                            title: 'Sucesso',
                            text: 'Cadastro realizado com sucesso',
                        })
                        $('#produtos_modal_novo_nome').val('');
                        $('#produtos_modal_novo_preco_custo').val('');
                        $('#produtos_modal_novo_preco_venda').val('');
                        produtos_table_get();
                    },
                    function (transaction, error) {
                        Swal.fire({
                            type: 'error',
                            title: 'Erro...',
                            text: 'Erro ao realizar o cadastro!',
                        });
                        $('#produtos_modal_novo').modal({
                            showClose: false
                        });
                    });
            });
        }
    });

});
