function notNull(val, text, title) {
    if (val != '' && val != null && val != undefined) {
        return true;
    } else {
        if (text != '' && text != null && text != undefined) {
            title = title != '' && title != null && title != undefined ? title : 'Erro!'
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: text,
            })
        }

        return false
    }
}